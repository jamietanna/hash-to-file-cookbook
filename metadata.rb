name 'hash-to-file'
maintainer 'Jamie Tanna'
maintainer_email 'chef@jamietanna.co.uk'
license 'Apache-2.0'
description 'Converts `Hash`es used for configuration into various file formats'
long_description 'Converts `Hash`es used for configuration into various file formats'
version '0.1.0'
chef_version '>= 12.1' if respond_to?(:chef_version)

issues_url 'https://gitlab.com/jamietanna/hash-to-file-cookbook/issues'
source_url 'https://gitlab.com/jamietanna/hash-to-file-cookbook'

%w(amazon arch centos debian fedora redhat ubuntu windows).each do |platform|
  supports platform
end
