# Description

Converts `Hash`es used for configuration into various file formats

# Requirements

## Platform:

* amazon
* arch
* centos
* debian
* fedora
* redhat
* ubuntu
* windows

## Cookbooks:

*No dependencies defined*

# Attributes

*No attributes defined*

# Recipes

*No recipes defined*

# Resources

* [json_file](#json_file) - Convert a given `Hash data` to a JSON file.
* [properties_file](#properties_file) - Convert a given `Hash data` to a properties file.
* [yaml_file](#yaml_file) - Convert a given `Hash data` to a yaml file.

## json_file

Convert a given `Hash data` to a JSON file.

### Actions

- create: Create the JSON file. Default action.

### Attribute Parameters

- path: The filepath that will be created
- data: The `Hash` that will be converted to JSON format
- owner: The owner on the file that will be created
- group: The group on the file that will be created
- mode: The mode on the file that will be created

## properties_file

Convert a given `Hash data` to a properties file.

### Actions

- create: Create the properties file. Default action.

### Attribute Parameters

- path: The filepath that will be created
- data: The `Hash` that will be converted to properties format
- owner: The owner on the file that will be created
- group: The group on the file that will be created
- mode: The mode on the file that will be created

## yaml_file

Convert a given `Hash data` to a yaml file.

### Actions

- create: Create the yaml file. Default action.

### Attribute Parameters

- path: The filepath that will be created
- data: The `Hash` that will be converted to yaml format
- owner: The owner on the file that will be created
- group: The group on the file that will be created
- mode: The mode on the file that will be created

# License and Maintainer

Maintainer:: Jamie Tanna (<chef@jamietanna.co.uk>)

Source:: https://gitlab.com/jamietanna/hash-to-file-cookbook

Issues:: https://gitlab.com/jamietanna/hash-to-file-cookbook/issues

License:: Apache-2.0
