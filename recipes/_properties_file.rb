properties_file 'create file' do
  path '/var/tmp/wibble.properties'
  data node['hash-to-file']['data']
  owner node['hash-to-file']['owner']
  group node['hash-to-file']['group']
  mode node['hash-to-file']['mode']
end
