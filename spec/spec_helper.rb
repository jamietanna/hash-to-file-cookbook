# frozen_string_literal: true
require 'chefspec'
require 'chefspec/berkshelf'

Dir['libraries/*.rb'].each { |f| require File.expand_path(f) }

def load_fixture(filename)
  path = File.join(File.dirname(__FILE__), "fixtures/#{filename}")
  File.read(path)
end
