require 'spec_helper'

describe Properties do
  let(:sut) { Properties.new }
  context '#convert(hash)' do
    it 'returns a sorted newline-joined string of key => value pairs' do
      data = {
        'b' => 'def hij',
        'a' => 'test this now',
      }
      expect(sut.convert(data)).to eq "a = test this now\nb = def hij"
    end

    it 'returns an empty string if given an empty hash' do
      expect(sut.convert({})).to eq ''
    end
  end

  context '#convert_kv(key, value)' do
    it 'handles regular key => value pairs' do
      expect(sut.convert_kv('key', 'value')).to eq('key = value')
    end

    it 'handles spaces in key names' do
      expect(sut.convert_kv('key with spaces', 'value')).to eq('key\ with\ spaces = value')
    end

    it 'joins arrays with commas' do
      expect(sut.convert_kv('another_key', %w(abc def geh))).to eq('another_key = abc,def,geh')
    end
  end
end
