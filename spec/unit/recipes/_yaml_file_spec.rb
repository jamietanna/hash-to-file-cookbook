require 'spec_helper'

describe 'hash-to-file::_yaml_file' do
  context 'When given a hash' do
    let(:data) do
      {
        'a' => [1, 2, 3],
        'b' => {
          'c' => 'd',
        },
      }
    end

    let(:chef_run) do
      runner = ChefSpec::SoloRunner.new do |node|
        node.automatic['hash-to-file']['data'] = data
        node.automatic['hash-to-file']['owner'] = 'own'
        node.automatic['hash-to-file']['group'] = 'grp'
        node.automatic['hash-to-file']['mode'] = '0123'
      end
      runner.converge(described_recipe)
    end

    it 'creates the yaml file' do
      expect(chef_run).to create_yaml_file('create file')
        .with(path: '/var/tmp/wibble.yaml')
        .with(data: data)
        .with(owner: 'own')
        .with(group: 'grp')
        .with(mode: '0123')
    end
  end
end
