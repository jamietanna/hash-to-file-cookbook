require 'spec_helper'

describe 'hash-to-file::_properties_file' do
  context 'When given a hash' do
    let(:data) do
      {
        'a' => [1, 2, 3],
        'b' => {
          'c' => 'd',
        },
      }
    end

    let(:chef_run) do
      runner = ChefSpec::SoloRunner.new do |node|
        node.automatic['hash-to-file']['data'] = data
        node.automatic['hash-to-file']['owner'] = 'def'
        node.automatic['hash-to-file']['group'] = 'fgh'
        node.automatic['hash-to-file']['mode'] = '1234'
      end
      runner.converge(described_recipe)
    end

    it 'creates the JSON file' do
      expect(chef_run).to create_properties_file('create file')
        .with(path: '/var/tmp/wibble.properties')
        .with(data: data)
        .with(owner: 'def')
        .with(group: 'fgh')
        .with(mode: '1234')
    end
  end
end
