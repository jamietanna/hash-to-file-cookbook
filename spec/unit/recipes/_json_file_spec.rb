require 'spec_helper'

describe 'hash-to-file::_json_file' do
  context 'When given a hash' do
    let(:data) do
      {
        'a' => [1, 2, 3],
        'b' => {
          'c' => 'd',
        },
      }
    end

    let(:chef_run) do
      runner = ChefSpec::SoloRunner.new do |node|
        node.automatic['hash-to-file']['data'] = data
        node.automatic['hash-to-file']['owner'] = 'own'
        node.automatic['hash-to-file']['group'] = 'grp'
        node.automatic['hash-to-file']['mode'] = '0123'
      end
      runner.converge(described_recipe)
    end

    it 'creates the JSON file' do
      expect(chef_run).to create_json_file('create file')
        .with(path: '/var/tmp/wibble.json')
        .with(data: data)
        .with(owner: 'own')
        .with(group: 'grp')
        .with(mode: '0123')
    end
  end
end
