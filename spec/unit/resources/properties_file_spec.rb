require 'spec_helper'

describe 'hash-to-file::_properties_file' do
  context 'When given a hash' do
    let(:data) do
      {
        'zed' => 'ex',
        'list' => [1, 2, 3],
        'key with spaces' => 'values with spaces',
        'key' => 'value',
        'abc' => 'def',
      }
    end

    let(:chef_run) do
      runner = ChefSpec::SoloRunner.new(step_into: ['properties_file']) do |node|
        node.automatic['hash-to-file']['data'] = data
        node.automatic['hash-to-file']['owner'] = 'abc'
        node.automatic['hash-to-file']['group'] = 'def'
        node.automatic['hash-to-file']['mode'] = '4567'
      end
      runner.converge(described_recipe)
    end

    it 'creates the JSON file' do
      expect(chef_run).to create_template('create file')
        .with(path: '/var/tmp/wibble.properties')
        .with(source: 'properties.erb')
        .with(variables: {
                data: data,
              })
        .with(owner: 'abc')
        .with(group: 'def')
        .with(mode: '4567')
      expect(chef_run).to(render_file('/var/tmp/wibble.properties')
        .with_content(load_fixture('wibble.properties')))
    end
  end
end
