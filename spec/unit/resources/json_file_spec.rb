require 'spec_helper'

describe 'hash-to-file::_json_file' do
  context 'When given a hash' do
    let(:data) do
      {
        'a' => [1, 2, 3],
        'b' => {
          'c' => 'd',
        },
      }
    end

    let(:chef_run) do
      runner = ChefSpec::SoloRunner.new(step_into: ['json_file']) do |node|
        node.automatic['hash-to-file']['data'] = data
        node.automatic['hash-to-file']['owner'] = 'own'
        node.automatic['hash-to-file']['group'] = 'grp'
        node.automatic['hash-to-file']['mode'] = '0123'
      end
      runner.converge(described_recipe)
    end

    it 'creates the JSON file' do
      expect(chef_run).to create_template('create file')
        .with(path: '/var/tmp/wibble.json')
        .with(source: 'json.erb')
        .with(variables: {
                data: data,
              })
        .with(owner: 'own')
        .with(group: 'grp')
        .with(mode: '0123')
      expect(chef_run).to(render_file('/var/tmp/wibble.json')
        .with_content(data.to_json))
    end
  end
end
