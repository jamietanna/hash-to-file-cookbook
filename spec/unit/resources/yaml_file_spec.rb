require 'spec_helper'

describe 'hash-to-file::_yaml_file' do
  context 'When given a hash' do
    let(:data) do
      {
        'a' => [1, 2, 3],
        'b' => {
          'c' => 'd',
        },
      }
    end

    let(:chef_run) do
      runner = ChefSpec::SoloRunner.new(step_into: ['yaml_file']) do |node|
        node.automatic['hash-to-file']['data'] = data
        node.automatic['hash-to-file']['owner'] = 'foo'
        node.automatic['hash-to-file']['group'] = 'bar'
        node.automatic['hash-to-file']['mode'] = '0000'
      end
      runner.converge(described_recipe)
    end

    it 'creates the yaml file' do
      expect(chef_run).to create_template('create file')
        .with(path: '/var/tmp/wibble.yaml')
        .with(source: 'yaml.erb')
        .with(variables: {
                data: data,
              })
        .with(owner: 'foo')
        .with(group: 'bar')
        .with(mode: '0000')
      expect(chef_run).to(render_file('/var/tmp/wibble.yaml')
        .with_content(load_fixture('wibble.yaml')))
    end
  end
end
