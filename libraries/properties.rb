class Properties
  def convert(hash)
    hash.sort.map do |k, v|
      convert_kv(k, v)
    end.join "\n"
  end

  def convert_kv(key, value)
    key = key.gsub(/ /, '\ ')
    value = value.join(',') if value.is_a? Array

    "#{key} = #{value}"
  end
end
