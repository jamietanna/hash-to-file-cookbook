=begin
#<
Convert a given `Hash data` to a properties file.

@action create  Create the properties file.
#>
=end
resource_name :properties_file

#<> @attribute path The filepath that will be created
property :path, String
#<> @attribute data The `Hash` that will be converted to properties format
property :data, Hash
#<> @attribute owner The owner on the file that will be created
property :owner, String
#<> @attribute group The group on the file that will be created
property :group, String
#<> @attribute mode The mode on the file that will be created
property :mode, String

default_action :create

action :create do
  template 'create file' do
    path new_resource.path
    source 'properties.erb'
    variables data: new_resource.data
    owner new_resource.owner
    group new_resource.group
    mode new_resource.mode
  end
end
